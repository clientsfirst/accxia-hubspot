(function($){
  var $window = $(window);
  var $body = $('body');
    var youtube_players = [];
    var vidyard_players = [];
    var $all_video_banners = $('.row-video-banner');
  var $video_background = $('.row-video-banner__background');
  
  var $ken_burns_banners = $('.parallax-bg--parallax-ken_burns');
  
  $(function(){
    if($.fn.parallax){
        /**
         * @author Phuc Pham
         * This doesn't work properly when section contains the Hubspot Form
         * In order to fix it, this needs to be reloaded on the callback onFormReady
         * https://legacydocs.hubspot.com/global-form-events
         */
      $('.parallax-bg__background[data-parallax]').parallax();
    }
  });
  

  $window.on('scroll.add_ken_burns', function() {
    for(var i=0; i<$ken_burns_banners.length; i++){
      var $element = $ken_burns_banners.eq(i);
      var top_of_element = $element.offset().top;  
      var bottom_of_screen = $window.scrollTop() + $window.innerHeight();

      if ( $element.hasClass('loaded-ken-burns') === false && (bottom_of_screen > top_of_element + 100)){
          $element.addClass('loaded-ken-burns');
      }
    }
  }); //.trigger('scroll.add_ken_burns');


  $(window).on('resize.resize_video_background', function(){
    $video_background.each(function(i, item){
      var $item = $(item);
      var $parent = $item.closest('.row-video-banner');
      var parent_w = $parent.width();
      var parent_h = $parent.height();
      var $fluid_wrapper = $item.find('.fluid-width-video-wrapper');
      var frame_w = $fluid_wrapper.outerWidth();
      var frame_h = $fluid_wrapper.outerHeight();
      
      var bg_w = parent_w;
      var bg_h = (frame_h/frame_w) * parent_w;
      
      if(bg_h < parent_h){
        bg_h = parent_h;
        bg_w = parent_h / (frame_h/frame_w);
      }
      
      $item.css('width', bg_w)
      .css('height', bg_h);
      
    });
  }).trigger('resize.resize_video_background');

    $all_video_banners.each(function(i, video_banner){
        var $video_banner = $(video_banner);

        $video_banner.bind('play_video', function(){
            $(this).addClass('play-video');
          $(window).trigger('resize.resize_video_background');
        });

        if($video_banner.hasClass('row-video-banner--type-vidyard')){
            var player_id = $video_banner.find('.row-video-banner__background img').prop('id');
            vidyard_players.push(player_id);
        }else if($video_banner.hasClass('row-video-banner--type-youtube')){
            var player_id = $video_banner.find('.row-video-banner__background iframe').prop('id');
            youtube_players.push(player_id);

        }else if($video_banner.hasClass('row-video-banner--type-vimeo')){
            var videoPlayer = new Vimeo.Player($video_banner.find('.iframe-player').get(0));
            videoPlayer.on('loaded', function() {
                $video_banner.find('.row-video-banner__background').fitVids();
                $video_banner.trigger('play_video');
                videoPlayer.play();
            });
        }

    });

    window.onVidyardAPICallbacks = window.onVidyardAPICallbacks || [];  
  window.onVidyardAPICallbacks.push(function(vidyardEmbed){
    for(var i = 0; i < vidyard_players.length;i++) {
      var $frame = $('#' + vidyard_players[i]);
      var $banner = $frame.closest('.video-banner');
      var uuid = $frame.data('uuid');
      
      if(uuid){
        vidyardEmbed.api.addReadyListener(function(_, player){
          if(player.ready()){
            $banner.find('.row-video-banner__background').fitVids();
            $banner.trigger('play_video');
            player.play();
          }
        }, uuid);
      }
        
    };
  });
  window.onVidyardAPI = function () {
      var args = arguments;

      window.onVidyardAPICallbacks.forEach(function (callback) {
          callback.apply(this, args)
      });
  };
  
    window.onYouTubeIframeAPIReadyCallbacks = window.onYouTubeIframeAPIReadyCallbacks || [];
    window.onYouTubeIframeAPIReadyCallbacks.push(function(){
        for(var i = 0; i < youtube_players.length;i++) {
            var $banner = $('#' + youtube_players[i]).closest('.row-video-banner');
            var player = new YT.Player(youtube_players[i], {
                events: {
                    'onReady': function(e){
                      $banner.find('.row-video-banner__background').fitVids();
                        $banner.trigger('play_video');
                      console.log('play video');
                    },
                    'onStateChange': function(){
                    }
                }
            });
        }

    });
  
    window.onYouTubeIframeAPIReady = function () {
        var args = arguments;

        window.onYouTubeIframeAPIReadyCallbacks.forEach(function (callback) {
            callback.apply(this, args)
        });
    };
  
})(jQuery);