jQuery(function($){
  
  /**
   * Fix issue addBack is not a function on jQuery 1.7
   */
  jQuery.fn.addBack = jQuery.fn.andSelf;

  $('.partner-carousel').each(function(i, item){
    var $item = $(item);
    var desktop = $item.data('desktop');
    var tablet = $item.data('tablet');
    var mobile = $item.data('mobile');
    var navigation = $item.data('navigation') === 'yes' ? 1 : 0;
    var pagination = $item.data('pagination') === 'yes' ? 1 : 0;
    var auto_rotate = $item.data('auto');
    
    $item.find('.owl-carousel').owlCarousel({
      loop:true,
      margin:0,
      responsiveClass:true,
      items: desktop,
      dots: pagination,
      nav: navigation,
      autoplay: auto_rotate ? 1 : 0,
      autoplayTimeout: auto_rotate ? auto_rotate : 5000,
      responsive:{
          0:{
              items: mobile
          },
          768:{
              items: tablet
          },
          992:{
              items: desktop
          }
      }
    });
  });
});